
reponame='drit.ml.git'
top=$(git rev-parse --show-toplevel)

gitdir=$(git rev-parse --absolute-git-dir)
find $gitdir -name "*~1" -delete
cd $gitdir
rm -rf refs/remotes/*
git --bare update-server-info
qmgit=$(ipfs add -r . -Q)
echo "qmgit: $qmgit"
cd $top
emptyd=$(ipfs object new -- unixfs-dir)
qmroot=$(ipfs object patch add-link $emptyd $reponame $qmgit)
echo "[remote ipfs]"
echo " url = https://ipfs.blockringtm.ml/ipfs/$qmroot/$reponame"

